package model.logic;

import API.IManager;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDateTime;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "Ruta Trips 2017-Q1 en directorio data";
	
	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "Ruta Trips 2017-Q2 en directorio data";
		
	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "Ruta Trips 2017-Q3 en directorio data";
			
	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "Ruta Trips 2017-Q4 en directorio data";
		
	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "Ruta Stations 2017-Q1-Q2 en directorio data";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "Ruta Stations 2017-Q3-Q4 en directorio data";
	
	public ICola<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

    public ILista<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

    public ILista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

    public ILista<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

    public ICola<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {
        return null;
    }

    public ILista<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

    public ILista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
        return null;
    }

    public ILista<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return null;
    }

	public void C1cargar(String rutaTrips, String rutaStations) {
		
	}
	
	public ICola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	public ILista<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		return null;
	}

	public ILista<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}
}
